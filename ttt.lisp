;; ttt.lisp
;;
;; Functions to help play a game of tic-tac-toe
;; Erik Steinmetz
;;

;; Just here as a handy board to test, in general don't use globals
(defparameter *brd* (list '- 'x '- 'o 'o 'o 'x '- '-))   ;; also setf

;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)))
      ((= i 9) nil)
      (if (= (mod i 3) 0)
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board)))

  (format t "~%============="))



;; Tests whether all three items in a list are equal to each other
(defun threequal (list)
  (and (equal (first list) (second list))
       (equal (second list) (third list))))


;; Grabs the nth row of a tic-tac-toe board as a list
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))))



;; Grabs the nth column of a tic-tac-toe board as a list
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))
