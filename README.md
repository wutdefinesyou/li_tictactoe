## Overview

A Tic-Tac-Toe implementation in LISP.

## Author

Zishuo Li

## File Listing
```
   |-- .gitignore
   |-- README.md
   |-- tictactoe.lisp
   |-- ttt.lisp
```

## How to run
Download [Clozure Common LISP](https://ccl.clozure.com/download.html). Run the executable in the root directory.
Clone the project and put *tictactoe.lisp* under the root directory of your ccl. Run ```(load "tictactoe.lisp")``` in the ccl terminal.
