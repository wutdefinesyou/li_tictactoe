; Tic-Tac-Toe
; Zishuo Li


;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "~%=============")
  (do ((i 0 (+ i 1)))
      ((= i 9) nil)
      (if (= (mod i 3) 0)
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board)))

  (format t "~%============="))

(defun board-init ()
  (loop repeat 9 collect '-))

;; Tests whether all three items in a list are equal to each other
; (defun threequal (list)
;   (and (equal (first list) (second list))
;        (equal (second list) (third list))))

(defun threequal-won (list)
  (or (and (equal (first list) 'x) (equal (second list) 'x) (equal (third list) 'x)) (and (equal (first list) 'o) (equal (second list) 'o) (equal (third list) 'o))))

;; Grabs the nth row of a tic-tac-toe board as a list
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))))

;; Grabs the nth column of a tic-tac-toe board as a list
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))

; Place a move on the board
(defun make-move (board pos)
  (if (yourturn board)
    (setf (nth pos board) 'o)
    (setf (nth pos board) 'x)))

; prompt a hint if not a legal move
(defun a-move-legal (board pos)
  (loop while (not (equal (nth pos board) '-))
    do (format t "~%Not a legal move. Please try another position:")
       (setf pos (read)))
  (make-move board pos))

; grab backslash diagnol
(defun grab-firstdiag (board)
  (list (nth 0 board) (nth 4 board) (nth 8 board)))

; grab slash diagonal
(defun grab-seconddiag (board)
  (list (nth 2 board) (nth 4 board) (nth 6 board)))

; (defun any-row-threequal (board)
;   (or (threequal(grab-row board 0)) (threequal(grab-row board 1)) (threequal(grab-row board 2))))
;
; (defun any-col-threequal (board)
;   (or (threequal(grab-col board 0)) (threequal(grab-col board 1)) (threequal(grab-col board 2))))
;
; (defun any-diag-threequal (board)
;   (or (threequal(grab-firstdiag board)) (threequal(grab-seconddiag board))))

; check if there is any three x or o in a row
(defun any-row-won (board)
  (or (threequal-won (grab-row board 0)) (threequal-won (grab-row board 1)) (threequal-won (grab-row board 2))))

; check if there is any three x or o in a column
(defun any-col-won (board)
  (or (threequal-won (grab-col board 0)) (threequal-won (grab-col board 1)) (threequal-won (grab-col board 2))))

; check if there is any three x or o in a diagnol
(defun any-diag-won (board)
  (or (threequal-won (grab-firstdiag board)) (threequal-won (grab-seconddiag board))))

; check if anyone won the gane
(defun won (board)
  (or (any-row-won board) (any-col-won board) (any-diag-won board)))

; recognize which player won
(defun who-win (board)
  (if (won board)
    (if (yourturn board)
      (format t "~%You win")
      (format t "~%Computer wins"))))

; which turn players are in
(defun turn-counter (board)
  (setq sum 0)
  (dolist (item board sum)
    (if (not (equal item '-))
      (incf sum 1))))

; true if it is player's turn, and false if it is opponent(computer)'s turn
(defun yourturn (board)
  (= (mod (turn-counter board) 2) 1))

; read a move from user input
(defun read-move (board)
  (format t "~%Place a move by entering any number from 0 to 8:")
  (setq temp (read))
  (loop while (or (< temp 0) (> temp 8))
    do (format t "~%Please enter a number from 0 to 8:")
    (setq temp (read)))
  (setq pos temp)
  (a-move-legal board pos)
  (print-board board)
  (did-win board))

; determines if the board is full
(defun ismovesleft (board)
  (do ((i 0 (+ i 1)))
      ((= i 9) nil)
      (if (equal (nth i board) '-)
        (return-from ismovesleft t)))
  (return-from ismovesleft nil))

; evaluate the move
(defun evaluate (board)
  (if (won board)
    (progn
      (if (yourturn board)
        (+ 10)
        (- 10)))
    (+ 0)))

; undo a move
(defun undo-move (board pos)
  (setf (nth pos board) '-))

; minimax algorithm
(defun minimax (board depth ismax)
  (setq score (evaluate board))
  (if (= score 10)
    (return-from minimax score))
  (if (= score (- 10))
    (return-from minimax score))
  (if (not (ismovesleft board))
    (return-from minimax 0))

  (cond
    (ismax
      (setq best (- 100))
      (do ((i 0 (+ i 1)))
          ((= i 9) nil)
          (if (equal (nth i board) '-)
             (progn
               (make-move board i)
               (setq best (max (best (minimax board (+ depth 1) (not ismax)))))
               (undo-move board i))))
      (return-from minimax best))
    ((not ismax
      (setq best (- 100))
      (do ((i 0 (+ i 1)))
          ((= i 9) nil)
          (if (equal (nth i board) '-)
             (progn
               (make-move board i)
               (setq best (min (best (minimax board (+ depth 1) (not ismax)))))
               (undo-move board i))))
      (return-from minimax best)))))

; find a best move with use of minimax
(defun findbestmove (board)
  (setq bestval (- 100))
  (setq bestmove (- 1))
  (do ((i 0 (+ i 1)))
      ((= i 9) nil)
      (if (equal (nth i board) '-)
        (progn
          (make-move board i)
          (setq moveval (minimax board 0 nil))
          (undo-move board i))
        (if (> moveval bestval)
          (progn
            (setq bestval moveval)
            (setq bestmove i)))))
  (return-from findbestmove bestmove))

; AI make a move
(defun ai-move (board)
  (if (> (findbestmove board) 0)
    (make-move board (findbestmove board))))

; tie if no one wins
(defun tie (board)
  (and (= (turn-counter board) 9) (not (won board))))

; render the game
(defun play (board)
  (loop while (not (won board))
    do  (if (tie board)
          (progn
            (format t "~%Tie")
            (return-from play (- 1))))
        (read-move board)
        (format t "~%Turn ~D" (turn-counter board))
        (ai-move board)
       (format t "~%Turn ~D" (turn-counter board)))
  (who-win board))

; driver
(defun test ()
  (setq myboard (board-init))
  (format t "~%Welcome to the Tic Tac Toe game")
  (print-board myboard)
  (play myboard))

(test)
